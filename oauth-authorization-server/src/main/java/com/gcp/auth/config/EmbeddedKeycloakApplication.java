package com.gcp.auth.config;

import com.gcp.auth.config.KeycloakServerProperties.AdminUser;
import org.keycloak.Config;
import org.keycloak.models.AdminRoles;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.RoleModel;
import org.keycloak.models.UserCredentialModel;
import org.keycloak.models.UserModel;
import org.keycloak.representations.idm.RealmRepresentation;
import org.keycloak.services.managers.RealmManager;
import org.keycloak.services.resources.KeycloakApplication;
import org.keycloak.util.JsonSerialization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;

public class EmbeddedKeycloakApplication extends KeycloakApplication {

    private static final Logger LOG = LoggerFactory.getLogger(EmbeddedKeycloakApplication.class);

    static KeycloakServerProperties keycloakServerProperties;

    public EmbeddedKeycloakApplication() {

        super();

        createDevRealm();

        createAdminUser();
    }

    private void createDevRealm() {
        executeInTransaction(session -> {
            RealmManager manager = new RealmManager(session);
            Resource lessonRealmImportFile = new ClassPathResource(keycloakServerProperties.getRealmImportFile());
            manager.importRealm(readJsonModel(lessonRealmImportFile, RealmRepresentation.class));
        });
    }

    private RealmRepresentation readJsonModel(Resource lessonRealmImportFile, Class<RealmRepresentation> clazz) {
        try {
            return JsonSerialization.readValue(getResourceInputStream(lessonRealmImportFile), clazz);
        } catch (IOException e) {
            throw new IllegalStateException("Error parsing JSON");
        }
    }

    private InputStream getResourceInputStream(Resource resource) {
        try {
            return resource.getInputStream();
        } catch (IOException e) {
            throw new IllegalStateException("Error accessing file stream");
        }
    }

    private void createAdminUser() {
        AdminUser admin = keycloakServerProperties.getAdminUser();

        executeInTransaction(session -> {
            RealmModel realm = session.realms().getRealm(Config.getAdminRealm());
            session.getContext().setRealm(realm);
            UserModel adminUser = session.users().addUser(realm, admin.getUsername());
            adminUser.setEnabled(true);
            UserCredentialModel usrCredModel = UserCredentialModel.password(admin.getPassword());
            session.userCredentialManager().updateCredential(realm, adminUser, usrCredModel);
            RoleModel adminRole = realm.getRole(AdminRoles.ADMIN);
            adminUser.grantRole(adminRole);
        });
    }

    protected void executeInTransaction(Consumer<KeycloakSession> operation) {
        KeycloakSession session = getSessionFactory().create();
        try {
            session.getTransactionManager().begin();
            operation.accept(session);
            session.getTransactionManager().commit();
        } catch (Exception ex) {
            LOG.warn("Failed to execute operation in transaction mode");
            session.getTransactionManager().rollback();
        } finally {
            session.close();
        }
    }
}
